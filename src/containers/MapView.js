import React, { Component } from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import { connect } from 'react-redux';
import { fetchLiveScootersOnField } from '../actions';


const GOOGLE_API_KEY = 'AIzaSyD9MVTQwAHND2mbaOs7e2w6oCByg8FsL7o';
const coords = { lat: 1.352083, lng: 103.81983600000001 };
const MAP_WIDTH = 420;
const MAP_HEIGHT = 300;
const MAP_ZOOM = 10.8;

class MapView extends Component {

  state = {
    showingInfoWindow: false,
    activeMarker: {},
    batteryPercentage: '',
    scooterId: '',
    activeMarkerposition: '',
    refresh: false
  };

  componentWillMount() {
    this.props.fetchLiveScootersOnField();
  }

  onMarkerClick = (props, marker, e) => {
    this.setState({
      batteryPercentage: props.batteryPercentage,
      scooterId: props.scooterId,
      activeMarker: marker,
      showingInfoWindow: true,
      activeMarkerposition: props.position
    });
  }

  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };

  showScooterMarkers() {
    if(!this.props.points)
      return
    else
      return this.props.points.map((markers) => <Marker key={markers.scooterId} position={{lat: markers.lat, lng: markers.lng }} />)
  }

  showLiveScooterMarkers() {
    if(!this.props.liveScootersArray)
      return
    else
      return this.props.liveScootersArray.map((markers) =>
        <Marker
          key={markers.scooterId}
          position={{ lat: markers.lat, lng: markers.lng }}
          onClick={this.onMarkerClick}
          batteryPercentage={markers.batteryPercentage}
          scooterId={markers.scooterId}
        />
      )
  }

  render() {
    if(this.props.type === 'local')
      return (
        <div>
          <div style={{ width: MAP_WIDTH, height: MAP_HEIGHT }}>
            <Map
              initialCenter={coords}
              google={this.props.google}
              style={{width: MAP_WIDTH, height: MAP_HEIGHT }}
              zoom={MAP_ZOOM}
            >
              {this.showScooterMarkers()}
            </Map>
          </div>
          <div>
            Local Map For Marker Presentation
          </div>
        </div>
    );
    if(this.props.type === 'live') {
      return (
        <div>
          <div style={{ width: MAP_WIDTH, height: MAP_HEIGHT }}>
            <Map
              initialCenter={coords}
              google={this.props.google}
              style={{width: MAP_WIDTH, height: MAP_HEIGHT }}
              zoom={MAP_ZOOM}
              onClick={this.onMapClicked}
            >
              {this.showLiveScooterMarkers()}
              <InfoWindow
                marker={this.state.activeMarker}
                visible={this.state.showingInfoWindow}>
                  <div>
                    <h5>Scooter Description</h5>
                    Scooter Id: {this.state.scooterId}<br />
                    Battery Status: {this.state.batteryPercentage}%<br />
                    Latitude: {this.state.activeMarkerposition.lat}<br />
                    Longitude: {this.state.activeMarkerposition.lng}
                  </div>
              </InfoWindow>
            </Map>
          </div>
          <div>
            Live Map With Active Scooters: {this.props.liveScootersArray.length} Scooters<br />
            <p style={{ fontSize: '12px'}}>Note: This is interactive map. Click on markers to get more info on them.</p>
          </div>
        </div>
      )
    }
  }
}

const mapStateToProps = ({ geoLocation }) => {
  const { points, liveScooters } = geoLocation;
  const liveScootersArray = _.map(liveScooters, (val) => {
    return { ...val };
  });
  return { points, liveScootersArray };
}

export default connect(mapStateToProps, { fetchLiveScootersOnField })(GoogleApiWrapper({
  apiKey: (GOOGLE_API_KEY)
})(MapView))
