
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { calculateRandomGeoLocations, updateGeoLocationForm } from '../actions/';


class GeoLocationForm extends Component {

  state = { latitude: '', longitude: '', distanceInKm: '', noOfPoints: '' }

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    this.preLoadLatLng(nextProps.preloadArea);
  }

  preLoadLatLng(location) {
    switch(location) {
      case 'Downtown':
        this.setState({'latitude': 1.2996550034422687 });
        this.setState({'longitude': 103.84774021804334 });
        break;
      case 'Clementi':
        this.setState({'latitude': 1.3152404464043626 });
        this.setState({'longitude': 103.76450188457966 });
        break;
      case 'Boonlay':
        this.setState({'latitude': 1.3302118993650667 });
        this.setState({'longitude': 103.69304649531843 });
        break;
      case 'Sentosa':
        this.setState({'latitude': 1.2560796126828968 });
        this.setState({'longitude': 103.82017364725472 });
        break;
      case 'Bukit Merah':
        this.setState({'latitude': 1.2783272848721277 });
        this.setState({'longitude': 103.81159141659738 });
        break;
      case 'Yishun':
        this.setState({'latitude': 1.4283603378842076 });
        this.setState({'longitude': 103.83478984236717 });
        break;
      case 'Changi':
        this.setState({'latitude': 1.3460473649810898 });
        this.setState({'longitude': 103.96069407463075 });
        break;
      case 'Bedok':
        this.setState({'latitude': 1.3198653633056852 });
        this.setState({'longitude': 103.9298862218857 });
        break;
      default:
        return
    }
  }

  generateRandomScooterCoords() {
    const { latitude, longitude, distanceInKm, noOfPoints } = this.state;

    this.props.calculateRandomGeoLocations(
      {
        'lat':parseFloat(latitude),
        'lng':parseFloat(longitude),
        'distanceInKm': parseFloat(distanceInKm),
        'noOfPoints': parseFloat(noOfPoints)
      }
    );
  }

  render() {
    return (
      <div>
        <div style={{ display: 'flex', marginTop: '10px'}}>
          <div style={{ width: 150 }}>Enter Latitude:</div>
          <input
            value={this.state.latitude}
            onChange={(event) => {
              this.setState({ latitude: event.target.value });
            }}
          />
        </div>
        <div style={{ display: 'flex', marginTop: '10px'}}>
          <div style={{ width: 150 }}>Enter Longitude:</div>
          <input
            value={this.state.longitude}
            onChange={(event) => {
              this.setState({ longitude: event.target.value });
            }}
          />
        </div>
        <div style={{ display: 'flex', marginTop: '10px'}}>
          <div style={{ width: 150 }}>Enter No of Points:</div>
          <input
            value={this.state.numberOfPoints}
            onChange={(event) => {
              this.setState({ noOfPoints: event.target.value });
            }}
          />
        </div>
        <div style={{ display: 'flex', marginTop: '10px'}}>
          <div style={{ width: 150 }}>Enter Distance (km):</div>
          <input
            value={this.state.distanceInKm}
            onChange={(event) => {
              this.setState({ distanceInKm: event.target.value });
            }}
          />
        </div>
        <div style={{ marginTop: '10px' }}>
          <button onClick={() => this.generateRandomScooterCoords()}>
            Generate
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ geoLocation }) => {
  const { latitude, longitude, noOfPoints, distanceInKm } = geoLocation;
  return { latitude, longitude, noOfPoints, distanceInKm };
}

export default connect(mapStateToProps, { calculateRandomGeoLocations, updateGeoLocationForm })(GeoLocationForm);
