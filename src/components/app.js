import React, { Component } from 'react';
import GeoLocationForm from '../containers/GeoLocationForm';
import MapView from '../containers/MapView';
import { connect } from 'react-redux';
import { deployScootersToField, deleteLiveScooters, updateGeoLocationForm } from '../actions/';
import firebase from 'firebase';

class App extends Component {

  state = { preLoadArea: null }

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyDqU7fm8S7pRTgQQLygzg2zitvF-ozvqTY",
      authDomain: "bbeepscooter.firebaseapp.com",
      databaseURL: "https://bbeepscooter.firebaseio.com",
      projectId: "bbeepscooter",
      storageBucket: "bbeepscooter.appspot.com",
      messagingSenderId: "1014235366917"
    };
    firebase.initializeApp(config);
  }

  deployScootersToField(points) {
    if(!points)
      alert('No scooters to deploy.');
    else {
      if(points.length<=0)
        alert('No scooters to deploy. Please generate scooters first');
      else
        this.props.deployScootersToField(points);
    }
  }

  render() {
    return (
      <div>
        <div style={{ display: 'flex', margin: '20px', justifyContent: 'center' }}>
          <h1>Bbeep Field Scooter Mapper</h1>
        </div>

        <div style={{ margin: '5px', display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <GeoLocationForm preloadArea={this.state.preLoadArea}/>
          </div>
          <div>
            <h6>Preload Coordinates Of Important Locations: -</h6>
            <div style={{ display: 'flex' }}>
              <ul>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Downtown'})}>Downtown</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Clementi'})}>Clementi</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Boonlay'})}>Boonlay</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Sentosa'})}>Sentosa</button></li>
              </ul>
              <ul>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Bukit Merah'})}>Bukit Merah</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Yishun'})}>Yishun</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Changi'})}>Changi</button></li>
                <li style={{ margin: '10px'}}><button onClick={() => this.setState({ preLoadArea: 'Bedok'})}>Bedok</button></li>
              </ul>
            </div>
          </div>
        </div>

        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ marginTop: '40px', border: '1px solid grey' }}>
            <MapView type={'local'}/>
          </div>
          <div style={{ marginTop: '40px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div style={{ width: '220px', justifyContent:'center', position: 'relative' }}>
              <button style={{ width: '220px' }} onClick={() => this.deployScootersToField(this.props.points)}>
                Load Scooters to field >>>
              </button>
              <button style={{ marginTop: '20px', width: '220px' }} onClick={() => this.props.deleteLiveScooters()}>
                Delete All Live Scooters
              </button>
            </div>
          </div>
          <div style={{ marginTop: '40px', border: '1px solid grey' }}>
            <MapView type={'live'}/>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ geoLocation }) => {
  const { points, updateGeoLocationForm } = geoLocation;
  return { points, updateGeoLocationForm }
}


export default connect(mapStateToProps, { deployScootersToField, deleteLiveScooters, updateGeoLocationForm })(App);
