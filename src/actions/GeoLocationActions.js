import _ from 'lodash';
import {
  CALCULATE_RANDOM_GEO_LOCATIONS,
  UPDATE_GEO_LOCATION_FORM,
  FETCH_SCOOTERS_SUCCESS,
  CLEAR_LOCAL_MAP
} from './types';
import firebase from 'firebase';

const uuidv4 = require('uuid/v4');
const v4options = {
  random: [
    0x10, 0x91, 0x56, 0xbe, 0xc4, 0xfb, 0xc1, 0xea,
    0x71, 0xb4, 0xef, 0xe1, 0x67, 0x1c, 0x58, 0x36
  ]
};

export const calculateRandomGeoLocations = ({ lat, lng, noOfPoints, distanceInKm }) => {
  var points = [];
  for (var i=0; i<noOfPoints; i++) {
    points.push(generateRandomPoint({lat, lng}, distanceInKm));
  }

  const randomScooterPoint = { 'lat': 1.290684,'lng': 103.849695 };
  const latMax = _.maxBy(points, 'lat').lat;
  const latMin = _.minBy(points, 'lat').lat;
  const lngMax = _.maxBy(points, 'lng').lng;
  const lngMin = _.minBy(points, 'lng').lng

  console.log('Max value of latitude: ', _.maxBy(points, 'lat'));
  console.log('Min value of latitude: ', _.minBy(points, 'lat'));
  console.log('Max value of Longitude: ', _.maxBy(points, 'lng'));
  console.log('Min value of Longitude: ', _.minBy(points, 'lng'));

  if((randomScooterPoint.lat<=latMax && randomScooterPoint.lat>=latMin) && (randomScooterPoint.lng<=lngMax && randomScooterPoint.lng>=lngMin)) {
    console.log('Point is inside 1km radius');
  } else {
    console.log('Point is outside 1km radius');
  }

  return {
    type: CALCULATE_RANDOM_GEO_LOCATIONS,
    payload: points
  }
}

export const updateGeoLocationForm = ({ prop, value }) => {
  return {
    type: UPDATE_GEO_LOCATION_FORM,
    payload: {prop, value}
  }
}

export const clearLocalMap = () => {
  return {
    type: CLEAR_LOCAL_MAP
  }
}

export const deployScootersToField = (scooters) => {
  return () => {
    if(scooters.length>100)
      alert('Max 100 scooters allowed to be deployed at one time.')
    else {
      _.forEach(scooters, (scooter) => {
        const { scooterId, lat, lng, batteryPercentage } = scooter;
        firebase.database().ref(`/scooters/${scooterId}`)
        .set({ scooterId, lat, lng, batteryPercentage })
        .catch((error) => console.log(error));
      })
    }
  }
}

export const fetchLiveScootersOnField = () => {
  return (dispatch) => {
    firebase.database().ref('/scooters/')
    .on('value', (snapshot) => {
      dispatch({ type: FETCH_SCOOTERS_SUCCESS, payload: snapshot.val()})
    })
  }
}

export const deleteLiveScooters = () => {
  return (dispatch) => {
    firebase.database().ref('/scooters/')
    .remove()
  }
}

const generateRandomPoint = (center, maxDist) => {

  //Defining geographic constants
  const PI = 3.1415926533;
  const EARTH_RADIUS = 6372.796924;

  //Convert Coordinates to Radians
  var startLat = center.lat * (PI/180);
  var startLon = center.lng * (PI/180);

  //Rand1 and Rand2 are two random numbers with values always between 0 & 1
  var rand1 = Math.random();
  var rand2 = Math.random();

  //Convert maxDist to Radians
  maxDist = maxDist/EARTH_RADIUS;

  /*Compute a random distance from 0 to maxdist scaled so that points on
  //larger circles have a greater probability of being chosen than points on smaller
  circles as described earlier.*/
  //Math.acos(rand1*(Math.cos(maxDist) - 1) + 1)
  const randomDist = Math.acos(rand1*(Math.cos(maxDist) - 1) + 1);

  /*Compute a random bearing from 0 to 2*PI radians (0 to 360 degrees),
  with all bearings having an equal probability of being chosen.*/
  const bearings = 2*PI*rand2;

  /*Use the starting point, random distance and random bearing to
  calculate the coordinates of the final random point.*/
  const resultLat = Math.asin(Math.sin(startLat)*Math.cos(randomDist) + Math.cos(startLat)*Math.sin(randomDist)*Math.cos(bearings));
  let resultLon = startLon + Math.atan2(Math.sin(bearings)*Math.sin(randomDist)*Math.cos(startLat), Math.cos(randomDist) - Math.sin(startLat)*Math.sin(resultLat));

  if(resultLon<-PI) {
    resultLon = resultLon + 2*PI;
  }

  if(resultLon>PI) {
    resultLon = resultLon - 2*PI;
  }

  //Generate Random Battery Percentage
  const batteryPercentage = parseInt(rand1*100);

  //Generate Random Scooter ID
  const scooterId = parseInt(rand2*1000);

  return { 'scooterId': 'bbeep00xbSG'+uuidv4().toString() ,'lat': resultLat*(180/PI), 'lng': resultLon*(180/PI), 'bearing': bearings*(180/PI), 'batteryPercentage': batteryPercentage };
}
