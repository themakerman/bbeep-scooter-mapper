export const CALCULATE_RANDOM_GEO_LOCATIONS = 'calculate_random_geo_locations';
export const UPDATE_GEO_LOCATION_FORM = 'update_geo_location_form';
export const FETCH_SCOOTERS_SUCCESS = 'fetch_scooters_success';

export const CLEAR_LOCAL_MAP = 'clear_local_map';
