import {
  CALCULATE_RANDOM_GEO_LOCATIONS,
  UPDATE_GEO_LOCATION_FORM,
  FETCH_SCOOTERS_SUCCESS,
  CLEAR_LOCAL_MAP
} from '../actions/types';

const INITIAL_STATE = { latitude: '', longitude: '', noOfPoints: '', distanceInKm: '', points: null, liveScooters: null };

const geoLocationReducer = (state=INITIAL_STATE, actions) => {
  switch(actions.type) {
    case UPDATE_GEO_LOCATION_FORM:
      return { ...state, [actions.payload.prop]: actions.payload.value }
    case CLEAR_LOCAL_MAP:
      return { ...state, points: null }
    case CALCULATE_RANDOM_GEO_LOCATIONS:
      return { ...state, points: actions.payload }
    case FETCH_SCOOTERS_SUCCESS:
      return { ...state, liveScooters: actions.payload }
    default:
      return { ...state };
  }
}

export default geoLocationReducer;
